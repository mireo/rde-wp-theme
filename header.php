<div class="menuLayer">
    <a class="hideMenu">
        <span class="icon fa fa-times"></span>
    </a>
    <div class="menuLayerContainer">
        <div class="menuLayerContainerMiddle">
            <div class="menuLayerWrapper">
                <div class="wrapperLimited">
                    <div class="wrapperBig">
                            <?php wp_nav_menu( array(
                                'theme_location' => 'mobile',
                                'menu_class' => '',
                                'container_class' => 'row',
                                'items_wrap' => '%3$s',
                                'walker' => new Mobile_Menu_Walker()
                            ) ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="header">
    <div class="wrapperBig">
        <div class="columnLeft">
            <div class="logo">
                <a href="<?php echo pll_home_url(  );//pll_current_language('slug');?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/logo.svg" alt="Rent Deejay Equipment"></a>
            </div>
            <?php if( $languages = PLL()->links->model->get_languages_list() ):?>
            <div class="lang">
                <a class="currentLang"><?php echo strtoupper(pll_current_language('slug'));?> <span class="fa fa-sort-desc"></span></a>
                <div class="submenu">
                    <div class="submenuContainer">
                        <div class="triangle"></div>
                        <ul>
                            <?php foreach($languages as $language):?>
                            <li><a href="<?php echo Pll()->links->get_translation_url( $language );?>"><?php echo strtoupper($language->slug);?></a></li>
                            <?php endforeach;?>
<!--                            <li><a href="#">NE</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>
            <?php endif;?>
        </div>
        <div class="columnRight">
            <?php wp_nav_menu( array(
                    'theme_location' => 'top',
                    'menu_class' => '',
                    'container_class' => 'nav',
                    'walker' => new Top_Menu_Walker()
            ) ); ?>
            <?php if( function_exists('woocommerce_mini_cart') ) woocommerce_mini_cart();?>
            <?php wp_nav_menu( array(
                'theme_location' => 'social',
                'menu_class' => '',
                'container_class' => 'social',
                'items_wrap' => '%3$s',
                'walker' => new Top_Menu_Social_Walker()
            ) ); ?>
            <div class="mobileNew">
                <a class="showMenu">
                    <span class="icon fa fa-bars"></span>
                </a>
            </div>
        </div>
        <div class="clearBoth"></div>
    </div>
</div>