<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/bootstrap.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/bootstrap-social.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/bootstrap-reboot.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/bootstrap-grid.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/font-awesome.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/slick.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/slick-theme.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/fancybox.css" type="text/css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/lity.css" type="text/css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/animate.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/rde.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css" >
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/Public/styles.css?v=2222222" type="text/css" />
<script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/tether.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/modernizr.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/masonry.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/isotope.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/fancybox.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/lity.js"></script>
<script src="<?php echo get_template_directory_uri();?>/assets/js/rde.js"></script>

<?php wp_head(); ?>