<?php
/* Template Name: Transport Page */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php echo get_template_part( 'head' );?>
</head>

<body <?php body_class(); ?>>

<?php get_header(); ?>

<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <?php
            if( have_posts() )
                while ( have_posts() ):the_post();?>
            <div class="section">
                <div class="sectionHeadline">
                    <div class="sectionHeadlineRow">
                        <div class="columnLeft">
                            <h2><?php the_title();?></h2>
                        </div>
                    </div>
                </div>
                <div class="textpageContent">
                    <div class="row">
                        <div class="col"><?php the_post_thumbnail('full');?></div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="lead">
                                <?php the_content();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php wp_reset_postdata(); endwhile;?>
        </div>
    </div>
</div>

<?php get_footer();?>
</body>
</html>
