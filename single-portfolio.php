<?php
/**
 * The template for displaying single post
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <?php echo get_template_part( 'head' );?>
    </head>

<body <?php body_class(); ?>>

<?php get_header(); ?>

    <div class="mainContent subpage">
        <div class="wrapperLimited">
            <div class="wrapper">
                <?php
                /* Start the Loop */
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/post/content', 'portfolio' );

                endwhile;
                ?>

            </div>
        </div>
    </div>
<?php get_footer();?>
</body>
</html>
