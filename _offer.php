<?php
/* Template Name: Offer Page */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php echo get_template_part( 'head' );?>
</head>

<body <?php body_class(); ?>>

<?php get_header(); ?>

<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <div class="section latestProducts">
                <div class="sectionHeadline filterHeadline">
                    <div class="sectionHeadlineRow">
                        <div class="columnLeft">
                            <h3>Deejay Equipment</h3>
                        </div>
                        <div class="columnRight">
                            <div id="filters" class="button-group">
                                <div class="buttonContainer"><button class="button is-checked" data-filter="*">All</button></div>
                                <div class="buttonContainer"><button class="button" data-filter=".cdusb">CD/USB</button></div>
                                <div class="buttonContainer"><button class="button" data-filter=".mixer">Mixer</button></div>
                                <div class="buttonContainer"><button class="button" data-filter=".turntable">Turntable</button></div>
                                <div class="buttonContainer"><button class="button" data-filter=".systems">Systems</button></div>
                                <div class="buttonContainer"><button class="button" data-filter=".controlers">Controlers</button></div>
                                <div class="buttonContainer"><button class="button" data-filter=".other">Other</button></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="itemsGrid">
                    <div class="grid-sizer"></div>
                    <div class="grid-item cdusb">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct1.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">CD/USB</div>
                                <div class="title"><a href="#">Pioneer CDJ 2000 NXS2</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item cdusb">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct1.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">CD/USB</div>
                                <div class="title"><a href="#">Pioneer CDJ 2000 NXS2</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item turntable">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart added">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct2.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Turntable</div>
                                <div class="title"><a href="#">Pioneer PLX-1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item cdusb">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct1.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">CD/USB</div>
                                <div class="title"><a href="#">Pioneer CDJ 2000 NXS2</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item turntable">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart added">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct2.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Turntable</div>
                                <div class="title"><a href="#">Pioneer PLX-1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct3.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct4.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 900 NEXUS/SRT</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct3.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct4.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 900 NEXUS/SRT</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item turntable">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart added">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct2.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Turntable</div>
                                <div class="title"><a href="#">Pioneer PLX-1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item cdusb">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct1.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">CD/USB</div>
                                <div class="title"><a href="#">Pioneer CDJ 2000 NXS2</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item turntable">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart added">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct2.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Turntable</div>
                                <div class="title"><a href="#">Pioneer PLX-1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct4.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 900 NEXUS/SRT</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item turntable">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart added">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct2.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Turntable</div>
                                <div class="title"><a href="#">Pioneer PLX-1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item cdusb">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct1.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">CD/USB</div>
                                <div class="title"><a href="#">Pioneer CDJ 2000 NXS2</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item turntable">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <div class="labelNew">New</div>
                                <a class="addToCart added">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct2.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Turntable</div>
                                <div class="title"><a href="#">Pioneer PLX-1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct3.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct4.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 900 NEXUS/SRT</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct3.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 1000</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-item mixer">
                        <div class="itemContainer">
                            <div class="itemProduct">
                                <a class="addToCart">
                                    <span class="fa fa-shopping-cart"></span>
                                    <span class="fa fa-check"></span>
                                </a>
                                <div class="productThumb">
                                    <div class="productThumbContainer">
                                        <a href="#"><img src="images/temp/thumbProduct4.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="category">Mixer</div>
                                <div class="title"><a href="#">Pioneer DJM 900 NEXUS/SRT</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination">
                    <div class="paginationRow">
                        <div class="columnLeft">
                            <a href="#" class="prev"><span class="fa fa-chevron-left"></span>Poprzednia</a>
                        </div>
                        <div class="columnCenter">
                            <ul>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li class="current"><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>
                        </div>
                        <div class="columnRight">
                            <a href="#" class="next">Następna<span class="fa fa-chevron-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>
</body>
</html>
