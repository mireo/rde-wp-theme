/**
 * Created by mireo on 22.10.2017.
 */

module.exports = function(grunt) {

    grunt.initConfig({
        // requirejs: {
        //     build: {
        //         options: {
        //             name: 'bootstrap',
        //             optimize: 'uglify2',
        //             wrap: {
        //                 startFile: 'bower_components/requirejs/require.js'
        //             },
        //             baseUrl: 'src/js',
        //             mainConfigFile: 'src/js/bootstrap.js',
        //             out: '<%= conf.dir.scripts %>/script.js',
        //             preserveLicenseComments: false
        //         }
        //     }
        // },
        compass: {
            build: {
                "options": {
                    "httpPath": "/",
                    "cssDir": "<%= conf.dir.css %>",
                    "sassDir": "src/scss",
                    "imagesDir": "assets/images",
                    "javascriptsDir": "src/js",
                    "relativeAssets": true,
                    "outputStyle": "compressed",
                    "noLineComments": false
                }
            }
        },
        watch: {
            // scripts: {
            //     files: ['src/js/*.js', 'src/js/**/*.js'],
            //     tasks: ['requirejs:build']
            // },
            styles: {
                files: ['src/scss/*.scss', 'src/scss/**/*.scss'],
                tasks: ['compass:build']
            }
        }
    });

    var conf = grunt.file.readJSON('build/config.json');
    grunt.config('conf', conf);

    console.log('conf', conf);

    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-compass');

    grunt.registerTask('default', ['watch']);

    grunt.registerTask('build', [
        // 'requirejs:build',
        'compass:build'
    ]);

};