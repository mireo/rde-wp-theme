/**
 * Created by mczernik on 26.07.2017.
 */

define(['jquery', 'slick', 'masonry', 'isotope', 'fancybox'], function ($, slick, masonry, isotope, fancybox) {

    function Rde() {
        console.log('rde init');
        $(window).on('load', function () {

            $(window).resize();

        });

        jQuery(document).ready(function($){
            jQuery(document).on( 'added_to_cart', function(fragment, test, test2){
                jQuery.post(
                    woocommerce_params.ajax_url, // The AJAX URL
                    {'action': 'mode_theme_update_mini_cart'}, // Send our PHP function
                    function(response){
                        document.getElementById('mobile_cart_items').innerHTML = response;
                    }
                );
            });
        });

        $(window).ready(function() {

            $(window).resize();

            $(".tabs .button a").click(function(){
                $(".tabsContent .tab").hide();
                $(".tabs .button").removeClass("current");
                if ($(this).parent().hasClass("button1")) {
                    $(this).parent().addClass("current");
                    $(".tabsContent .tabContent1").show();
                }
                else if ($(this).parent().hasClass("button2")) {
                    $(this).parent().addClass("current");
                    $(".tabsContent .tabContent2").show();
                }
                else if ($(this).parent().hasClass("button3")) {
                    $(this).parent().addClass("current");
                    $(".tabsContent .tabContent3").show();
                }
            });

            $(".homeTop .homeTopContent .slider").slick({
                dots: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                swipeToSlide: true
            });

            $(".itemPortfolioSlider ul").each(function(i) {
                var el = $(this);
                el.slick({
                    dots: false,
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    swipeToSlide: false,
                    fade: false,
                    pauseOnHover: false
                });
            });

            // grid

            $( function() {
                $('.itemsGrid').isotope({
                    itemSelector: '.grid-item',
                    layoutMode: 'masonry',
                    masonry: {
                        columnWidth: '.grid-sizer'
                    }
                });
            });

            // filter

            $( function() {
                // init Isotope
                var $container = $('.itemsGrid').isotope({
                    itemSelector: '.grid-item',
                    layoutMode: 'masonry'
                });
                // filter functions
                var filterFns = {
                    // show if number is greater than 50
                    numberGreaterThan50: function() {
                        var number = $(this).find('.number').text();
                        return parseInt( number, 10 ) > 50;
                    },
                    // show if name ends with -ium
                    ium: function() {
                        var name = $(this).find('.name').text();
                        return name.match( /ium$/ );
                    }
                };
                // bind filter button click
                $('#filters').on( 'click', 'button', function() {
                    var filterValue = $( this ).attr('data-filter');
                    // use filterFn if matches value
                    filterValue = filterFns[ filterValue ] || filterValue;
                    $container.isotope({ filter: filterValue });
                });
                // change is-checked class on buttons
                $('.button-group').each( function( i, buttonGroup ) {
                    var $buttonGroup = $( buttonGroup );
                    $buttonGroup.on( 'click', 'button', function() {
                        $buttonGroup.find('.is-checked').removeClass('is-checked');
                        $( this ).addClass('is-checked');
                    });
                });
            });

            $('.itemsGrid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer'
            });

            $("a.addToCart").click(function(){
                if($(this).hasClass('added')){
                    $(this).removeClass("added");
                }
                else {
                    $(this).addClass("added");
                }
            });
            $(".addToCartButton a").click(function(){
                if($(this).hasClass('added')){
                    $(this).removeClass("added");
                }
                else {
                    $(this).addClass("added");
                }
            });

            $(".logos ul").slick({
                dots: false,
                infinite: true,
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: true,
                swipeToSlide: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 459,
                        settings: {
                            slidesToShow: 1,
                        }
                    }
                ]
            });

            $(".productMainGallery ul").slick({
                dots: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                arrows : false,
                swipeToSlide: true
            });

            $(".fancybox-effects-d").fancybox({
                padding: 0,
                openEffect : 'elastic',
                openSpeed  : 150,
                closeEffect : 'elastic',
                closeSpeed  : 150,
                closeClick : true,
                helpers : {
                    overlay : null
                }
            });


            function showMenu(){
                $(".menuLayer").addClass("active");
                $(".menuLayer").each(function(i) {
                    $(this).find(".mainLink").each(function(i) {
                        var el = $(this);
                        setTimeout(function() {
                            el.addClass('fadeInUp');
                        }, i * 100);
                    });
                    $(this).find(".subLink").each(function(i) {
                        var el = $(this);
                        setTimeout(function() {
                            setTimeout(function() {
                                el.addClass('fadeInUp');
                            }, i * 150);
                        }, 900);
                    });
                });
            }

            function hideMenu(){
                $(".menuLayer").removeClass("active");
                $(".menuLayer").each(function(i) {
                    $(this).find(".animated").removeClass('fadeInUp');
                });
            }

            $(".showMenu").click(function(){
                showMenu();
                // hideSearch();
            });

            $(".hideMenu").click(function(){
                hideMenu();
            });




        });

        $(window).scroll(function(){

            if ($(this).scrollTop() > 0) {
                $(".header").addClass('min');
            }
            else {
                $(".header").removeClass('min');
            }

        });

        $(window).resize(function(){


            if($('.homeTop').length > 0) {
                wrapperLeftPosition = ($(".mainContent .wrapperLimited").offset().left);
                $(".slick-dots").css({ 'left': wrapperLeftPosition + "px" });
            }

        });

    }

    Rde.prototype.init = function (callback) {
        console.log( 'rde init' );

    };

    Rde.prototype.destroy = function () {

    };

    return Rde();

});