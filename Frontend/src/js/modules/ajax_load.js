/**
 * Created by mczernik on 26.07.2017.
 */

define(['axios', 'jquery'], function (Axios, $) {

    function AjaxLoad() {
    }


    var offset = 250,
        trigger = true,
        scrollHandler;

    AjaxLoad.prototype.init = function (callback) {
        console.log( 'init ajax' );

        this.callback = callback || function () {
        };

        this.preloader = $('#page__preloader');

    };

    AjaxLoad.prototype.ajaxRequest = function (url) {

        var _this = this;

        if (url) {

            Axios.get(url)
                .then(function (response) {
                    var page = document.createElement('response');
                    page.innerHTML = response.data;
                    var node = page.querySelector('#content > section[data-next-page]');

                    document.getElementById('content').appendChild(node);
                    _this.preloader.hide();
                    // _this.preloader.removeClass('page__preloader--bottom');
                    trigger = true;

                });
        }
    };

    AjaxLoad.prototype.destroy = function () {

    };

    return AjaxLoad;

});