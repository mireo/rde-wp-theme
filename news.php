<?php
/* Template Name: News Page */
global $paged;
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <?php echo get_template_part( 'head' );?>
    </head>

<body <?php body_class(); ?>>

<?php get_header(); ?>

<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <div class="section latestProducts">
                <div class="sectionHeadline">
                    <div class="sectionHeadlineRow">
                        <h3><?php the_title();?></h3>
                    </div>
                </div>
                <div class="itemsGrid">
                    <div class="grid-sizer"></div>
                    <?php

                    $custom_args = array(
                        'post_type' => 'post',
                        'paged' => $paged
                    );
                    $custom_query = new WP_Query( $custom_args );
                    if( $custom_query->have_posts() ) {
                        while ($custom_query->have_posts()) {
                            $custom_query->the_post();
                            get_template_part('template-parts/post/content-teaser', get_post_format());
                        }
                        wp_reset_postdata();
                    } ?>
                </div>
                <?php
                if (function_exists('custom_pagination')) {
                    custom_pagination($custom_query->max_num_pages,"",$paged);
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>
</body>
</html>

