<?php

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php echo get_template_part( 'head' );?>
</head>

<body <?php body_class(); ?>>

<?php get_header(); ?>

<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <?php
                if( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();
                        the_content();
                    }
                    wp_reset_postdata();
                }
            ?>
        </div>
    </div>
</div>

<?php get_footer();?>
</body>
</html>
