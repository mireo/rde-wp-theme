<?php
/**
 * The template for displaying all single posts teaser
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <?php echo get_template_part( 'head' );?>
    </head>

<body
    <?php echo print_r('Index.php',1);exit;?>
    <?php body_class(); ?>>

<?php get_header(); ?>

<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <div class="section latestProducts">
                <div class="sectionHeadline">
                    <div class="sectionHeadlineRow">
                        <h3>News</h3>
                    </div>
                </div>
                <div class="itemsGrid">
                    <div class="grid-sizer"></div>
                    <?php
                    if( have_posts() ) {
                        while (have_posts()) {
                            the_post();
                            get_template_part('template-parts/post/content-teaser', get_post_format());
                        }
                    } ?>
                </div>
            </div>
            <div class="section fourColumns">
                <div class="sectionHeadline">
                    <h3>4 steps to order</h3>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="column">
                                    <div class="number">1</div>
                                    <div class="title">Find your products</div>
                                    <div class="desc">
                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="column">
                                    <div class="number">2</div>
                                    <div class="title">Add to order list</div>
                                    <div class="desc">
                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="column">
                                    <div class="number">3</div>
                                    <div class="title">Send a completed form</div>
                                    <div class="desc">
                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="column">
                                    <div class="number">4</div>
                                    <div class="title">Waiting for response</div>
                                    <div class="desc">
                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer();?>
</body>
</html>

