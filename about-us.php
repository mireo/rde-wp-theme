<?php
/* Template Name: About Us Page */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php echo get_template_part( 'head' );?>
</head>

<body <?php body_class(); ?>>

<?php get_header(); ?>

<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <?php if( have_posts() ):?>
                <?php while( have_posts() ): the_post();?>
            <div class="section">
                <div class="sectionHeadline">
                    <div class="sectionHeadlineRow">
                        <div class="columnLeft">
                            <h2><?php the_title();?></h2>
                        </div>
                    </div>
                </div>
                <div class="textpageContent">
                    <div class="row">
                        <div class="col">
                            <div class="lead">
                                <?php the_content();?>
                            </div>
                        </div>
                    </div>
                    <?php if( $columns = carbon_get_the_post_meta('columns') ):?>
                    <div class="row">
                        <?php foreach( $columns as $column ):?>
                        <div class="col-md-6">
                            <?php echo apply_filters('the_content', $column['content']);?>
                        </div>
                        <?php endforeach;?>
                    </div>
                    <?php endif;?>
                </div>
                <?php endwhile;?>
            </div>
            <?php endif;?>
        </div>
    </div>
</div>

<?php get_footer();?>
</body>
</html>
