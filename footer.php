<?php if ( is_active_sidebar( 'bottom-sidebar' ) ) : ?>
<div id="bottom-sidebar">
<!--    <div class="wrapperLimited">-->
<!--        <div class="wrapper">-->
<!--            <div class="section fourColumns">-->
<!--                <div class="sectionHeadline">-->
<!--                    <h3>4 steps to order</h3>-->
<!--                </div>-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-6">-->
<!--                        <div class="row">-->
<!--                            <div class="col-sm-6">-->
<!--                                <div class="column">-->
<!--                                    <div class="number">1</div>-->
<!--                                    <div class="title">Find your products</div>-->
<!--                                    <div class="desc">-->
<!--                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-6">-->
<!--                                <div class="column">-->
<!--                                    <div class="number">2</div>-->
<!--                                    <div class="title">Add to order list</div>-->
<!--                                    <div class="desc">-->
<!--                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-lg-6">-->
<!--                        <div class="row">-->
<!--                            <div class="col-sm-6">-->
<!--                                <div class="column">-->
<!--                                    <div class="number">3</div>-->
<!--                                    <div class="title">Send a completed form</div>-->
<!--                                    <div class="desc">-->
<!--                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-sm-6">-->
<!--                                <div class="column">-->
<!--                                    <div class="number">4</div>-->
<!--                                    <div class="title">Waiting for response</div>-->
<!--                                    <div class="desc">-->
<!--                                        Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis.-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <?php dynamic_sidebar( 'bottom-sidebar' ); ?>
</div>
<?php endif; ?>

<div class="footer">
    <div class="wrapperBig">
        <div class="footerRow">
            <div class="columnLeft"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo2.svg" alt="Rent Deejay Equipment"></a></div>
            <div class="columnRight">
                <?php wp_nav_menu( array(
                    'theme_location' => 'footer',
                    'menu_class' => '',
                    'container_class' => 'nav'
//                    'walker' => new Top_Menu_Walker()
                ) ); ?>

            </div>
            <div class="clearBoth"></div>
        </div>
        <div class="footerRow">
            <div class="columnLeft">
                © 2017 <a href="#">Rent Deejay Equipment</a>. All rights reserved
            </div>
            <div class="columnRight">Realization by <a class="olbromski" href="http://olbromski.com/" target="_blank" title="olbromski.com"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/olbromski.svg" alt="Olbromski Rafał"></a></div>
            <div class="clearBoth"></div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
