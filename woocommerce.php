<?php
/* Template Name: Woocommerce Template */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php echo get_template_part( 'head' );?>
</head>

<body <?php body_class(); ?>>

<?php get_header(); ?>



<?php
if ( is_singular( 'product' ) ) {

while ( have_posts() ) : the_post();

wc_get_template_part( 'content', 'single-product' );

endwhile;

} else { ?>
    <div class="mainContent subpage">
        <div class="wrapperLimited">
            <div class="wrapper">
                <div class="section latestProducts">
                    <div class="sectionHeadline filterHeadline">
                        <div class="sectionHeadlineRow">
                            <div class="columnLeft">
                                <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

                                    <h3><?php woocommerce_page_title(); ?></h3>

                                <?php endif; ?>
                            </div>
                            <div class="columnRight">
                                <?php //do_action( 'woocommerce_before_shop_loop' ); ?>
                                <?php
                                    $terms = get_terms_of_displayed_posts('product_cat', 10);
                                    if( $terms ):?>
                                <div id="filters" class="button-group">
                                    <div class="buttonContainer"><button class="button is-checked" data-filter="*"><?php _e('All', 'rde');?></button></div>
                                        <?php foreach( $terms as $term ):?>
                                    <div class="buttonContainer"><button class="button" data-filter=".<?php echo $term->slug;?>"><?php echo $term->name;?></button></div>
                                        <?php endforeach;?>
                                </div>
                                    <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="itemsGrid">
                        <div class="grid-sizer"></div>

                        <?php if ( have_posts() ) : ?>

                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php $type = '';?>
                                <?php
                                    if( $terms = get_the_terms( get_the_ID(), 'product_cat' ) ){
                                        if( 'deejays' == carbon_get_term_meta( $terms[0]->term_id, 'type' ) )
                                            $type = '-deejays';
                                    }
                                ?>
                                <?php wc_get_template_part( 'product'.$type, 'teaser' ); ?>

                            <?php endwhile; // end of the loop. ?>

                        <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                            <?php do_action( 'woocommerce_no_products_found' ); ?>

                        <?php endif;?>
                    </div>
                    <?php
                    if (function_exists('custom_pagination')) {
                        global $wp_query;
                        custom_pagination($wp_query->max_num_pages,"",$paged);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<?php get_footer();?>
</body>
</html>
