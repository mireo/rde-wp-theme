<?php
/**
 * The template for displaying single post
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
        <?php echo get_template_part( 'head' );?>
    </head>

<body <?php body_class(); ?>>

<?php get_header(); ?>

    <div class="mainContent subpage">
        <div class="wrapperLimited">
            <div class="wrapper">
                <?php
                /* Start the Loop */
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/post/content', get_post_format() );

                endwhile;
                ?>
                <div class="section relatedProducts">
                    <div class="sectionHeadline">
                        <div class="sectionHeadlineRow">
                            <div class="columnLeft">
                                <h3><?php _e('Latest news', 'rde');?></h3>
                            </div>
                        </div>
                    </div>
                    <?php
                    $args = array(
                        'posts_per_page' => 4,
                        'post__not_in' => array(get_the_ID())
                    );
                    $latest_posts = new WP_Query( $args );
                    if( $latest_posts->have_posts() ):?>
                    <div class="itemsGrid">
                        <div class="grid-sizer"></div>
                        <?php
                            while( $latest_posts->have_posts() ) {
                                $latest_posts->the_post();
                                get_template_part('template-parts/post/content-teaser', get_post_format());
                            }
                            wp_reset_postdata();
                        ?>
                    </div>
                    <?php endif;?>
                </div>

            </div>
        </div>
    </div>
<?php get_footer();?>
</body>
</html>
