<?php
/* Template Name: Contact Page */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php echo get_template_part( 'head' );?>
</head>

<body <?php body_class(); ?>>

<?php get_header(); ?>


<div class="mainContent subpage subpageContact">
    <div class="wrapperLimited">
        <div class="wrapper">
            <div class="section contact">
                <div class="sectionHeadline">
                    <div class="sectionHeadlineRow">
                        <div class="columnLeft">
                            <h2><?php the_title();?></h2>
                        </div>
                    </div>
                </div>
                <div class="row contactRow">
                    <?php if( $addresses = carbon_get_the_post_meta('addresses') ) foreach( $addresses as $address ):?>
                    <div class="col-xl-3">
                        <div class="contactTitle"><h4><?php echo $address['title'];?></h4></div>
                        <div class="contactDesc">
                            <?php echo apply_filters('the_content', $address['text']);?>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
                <div class="row contactRow">
                    <div class="col">
                        <div class="contactTitle"><h4><?php _e('Follow us', 'rde');?></h4></div>
                        <?php wp_nav_menu( array(
                            'theme_location' => 'social',
                            'menu_class' => '',
                            'container_class' => 'social',
                            'items_wrap' => '%3$s',
                            'walker' => new Contact_Social_Walker()
                        ) ); ?>
<!--                        <div class="social">-->
<!--                            <ul>-->
<!--                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>-->
<!--                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>-->
<!--                                <li><a href="#"><span class="fa fa-instagram"></span></a></li>-->
<!--                                <li><a href="#"><span class="fa fa-youtube-play"></span></a></li>-->
<!--                                <li><a href="#"><span class="fa fa-google-plus"></span></a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="row contactRow">
                    <?php if( $contacts = carbon_get_the_post_meta('contacts') ) foreach( $contacts as $key=>$contact ):?>
                    <div class="<?php echo ( isset($contact['class']) && $contact['class']!='')?$contact['class']:'col-md-4 col-lg-3';?>">
                        <div class="thumb">
                            <?php echo wp_get_attachment_image($contact['thumb'], 'thumbnail');?>
                        </div>
                        <div class="name"><?php echo $contact['title'];?></div>
                        <div class="subtitle"><?php echo $contact['company_title'];?></div>
                        <div class="contactDesc">
                            <?php echo apply_filters('the_content', $contact['text']);?>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer();?>
</body>
</html>
