<?php
/* Template Name: Front Page */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
    <head>
    <?php echo get_template_part( 'head' );?>
    </head>

    <body <?php body_class(); ?>>

        <?php get_header(); ?>

        <div class="videoBg">
            <div class="mask"></div>
            <?php
                if( $video_id = carbon_get_the_post_meta('video') ) {
                    echo '<video autoplay loop><source src="'.wp_get_attachment_url( $video_id ).'" type="video/mp4"></video>';
                }
            ?>
        </div>
<?php
$slides = carbon_get_the_post_meta( 'slides' );

if($slides):
?>
        <div class="homeTop">
            <div class="mask"></div>
            <div class="gradientMask"></div>
            <div class="homeTopContent">
                <div class="slider">
                    <?php foreach( $slides as $slider ):?>
                    <div>
                        <div class="slideTable">
                            <div class="slideColumn">
                                <div class="wrapperLimited">
                                    <div class="wrapper">
                                        <h1><?php echo $slider['title'];?></h1>
                                        <h2><?php echo $slider['text'];?></h2>
                                        <?php if( isset($slider['button']) ):?>
                                        <div class="buttons">
                                            <?php foreach( $slider['button'] as $button ):?>
                                                <?php switch( $button['_type'] ){
                                                    case 'simple':?>
                                                    <a class="showProducts" href="<?php echo $button['link'];?>"><?php echo $button['button_text'];?></a>
                                                    <?php break;
                                                    case 'oembed':?>
                                                    <a class="showVideo" href="<?php echo $button['uri'];?>" data-lity><span class="fa fa-play"></span> <?php echo $button['button_text'];?></a>
                                                    <?php break;
                                                    default:?>

                                                <?php } ?>
                                            <?php endforeach;?>
                                        </div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
<?php endif;?>
        <div class="mainContent">
            <div class="wrapperLimited">
                <div class="wrapper">
                    <?php
                    $args = array(
                        'posts_per_page' => 4,
                        'post_type' => 'product'
                    );
                    $latest_posts = new WP_Query( $args );
                    if( $latest_posts->have_posts() ):?>
                    <div class="section latestProducts">
                        <div class="sectionHeadline">
                            <div class="sectionHeadlineRow">
                                <div class="columnLeft">
                                    <h3><?php _e('Latest products', 'rde');?></h3>
                                </div>
                                <div class="columnRight">
                                    <div class="showAll">
                                        <a href="<?php if(function_exists('wc_get_page_id')) echo get_permalink( wc_get_page_id( 'shop' ) );?>"><?php _e('Show All', 'rde');?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="itemsGrid">
                            <div class="grid-sizer"></div>
                            <?php while ( $latest_posts->have_posts() ) : $latest_posts->the_post(); ?>
                                <?php $type = '';?>
                                <?php
                                if( $terms = get_the_terms( get_the_ID(), 'product_cat' )  ){
                                    if( is_array($terms) && 'deejays' == carbon_get_term_meta( $terms[0]->term_id, 'type' ) )
                                        $type = '-deejays';
                                }
                                ?>
                                <?php if(function_exists('wc_get_template_part')) wc_get_template_part( 'product'.$type, 'teaser' ); ?>

                            <?php endwhile; wp_reset_postdata();?>
                        </div>
                    </div>
                    <?php endif;?>
                    <?php
                    $args = array(
                        'posts_per_page' => 4,
                        'post__not_in' => array(get_the_ID())
                    );
                    $latest_posts = new WP_Query( $args );
                    if( $latest_posts->have_posts() ):?>
                    <div class="section latestNews">
                        <div class="sectionHeadline">
                            <div class="sectionHeadlineRow">
                                <div class="columnLeft">
                                    <h3><?php _e('Latest news','rde');?></h3>
                                </div>
                                <div class="columnRight">
                                    <?php
                                        $args = array(
                                            'posts_per_page' => 1,
                                            'post_type' => 'page',
                                            'meta_key' => '_wp_page_template',
                                            'meta_value' => 'news.php'
                                        );
                                        $news_page = get_posts($args);
                                        if( $news_page && isset($news_page[0]) ):
                                    ?>
                                    <div class="showAll">
                                        <a href="<?php echo get_permalink($news_page[0]);?>"><?php _e('Show All', 'rde');?></a>
                                    </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                        <div class="itemsGrid">
                            <div class="grid-sizer"></div>
                            <?php
                            while( $latest_posts->have_posts() ) {
                                $latest_posts->the_post();
                                get_template_part('template-parts/post/content-teaser', get_post_format());
                            }
                            wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>

        <?php get_footer();?>
    </body>
</html>
