<?php

if ( class_exists('WC_Shortcode_Cart') ) {


    class Rde_Cart extends WC_Shortcode_Cart
    {
        function __construct($atts = array())
        {
            $this::output($atts);
        }

        public static function output($atts)
        {
            // Constants.
            wc_maybe_define_constant('WOOCOMMERCE_CART', true);

            $atts = shortcode_atts(array(), $atts, 'woocommerce_cart');

            // Update Shipping
            if (!empty($_POST['calc_shipping']) && wp_verify_nonce($_POST['_wpnonce'], 'woocommerce-cart')) {
                self::calculate_shipping();

                // Also calc totals before we check items so subtotals etc are up to date
                WC()->cart->calculate_totals();
            }

            // Check cart items are valid
            do_action('woocommerce_check_cart_items');

            // Calc totals
            WC()->cart->calculate_totals();

            if (WC()->cart->is_empty()) {
                wc_get_template('cart/cart-empty.php');
            } else {
                wc_get_template('cart/cart.php');
            }
        }
    }
}