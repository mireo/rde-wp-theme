<?php
function create_posttype() {

    register_post_type( 'portfolio',
        array(
            'labels' => array(
                'name' => __( 'Portfolio', 'rde' ),
                'singular_name' => __( 'Portfolio', 'rde' )
            ),
            'public' => true,
            'publicly_queryable' => true,
            'rewrite' => false,
            'menu_icon' => 'dashicons-media-interactive',
            'menu_position' => 5
        )
    );

    $args = array(
        'hierarchical'      => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'portfolio_category' ),
    );

    register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

    $args = array(
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'portfolio_tag' ),
    );

    register_taxonomy( 'portfolio_tag', array( 'portfolio' ), $args );

    $args = array(
        'labels'             => array(
            'name' => __('Dates', 'rde'),
            'singular_name' => __('Date', 'rde'),
            'search_items'  => __('Search date', 'rde')
        ),
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'portfolio_date' ),
    );

    register_taxonomy( 'portfolio_date', array( 'portfolio' ), $args );
}

add_action( 'init', 'create_posttype' );