<?php
use Carbon_Fields\Widget;
use Carbon_Fields\Field;


class ClientsWidget extends Widget {
    function __construct() {
        $this->setup( 'clients_widget', __('Clients widget slider', 'rde'), __('Widget displaing clients carousel', 'rde'), array(
            Field::make( 'complex', 'client', __('Clients', 'rde') )
                ->set_layout( 'tabbed-vertical' )
                ->add_fields( array(
                    Field::make( 'text', 'url', __('Url to','rde') ),
                    Field::make( 'image', 'logo', __('Logo', 'rde') ) // We're only changing the label field to an image one
                    ->set_required(),
                ) )
        ) );
    }

    function front_end( $args, $instance ) {
        if( isset($instance['client']) && is_array($instance['client']) ){
            echo '<div class="logos"><ul>';
            foreach( $instance['client'] as $client ){
                echo '<li>';
                if( isset($client['url']) && $client['url'] != '' )
                    echo '<a href="'.$client['url'].'" target="_blank">';
                echo wp_get_attachment_image($client['logo'], 'logo-h90');
                if( isset($client['url']) && $client['url'] != '' )
                    echo '</a>';
                echo '</li>';
            }
            echo '</ul></div>';
        }
    }
}

class StepsWidget extends Widget{
    function __construct() {
        $this->setup( 'steps_widget', __('Steps widget', 'rde'), __('Widget displaing steps', 'rde'), array(
            Field::make( 'text', 'title', __('Title', 'rde') ),
            Field::make( 'complex', 'steps', __('Steps', 'rde') )
                ->set_layout( 'tabbed-vertical' )
                ->add_fields( array(
                    Field::make( 'text', 'title', __('Title','rde') )
                        ->set_required(),
                    Field::make( 'textarea', 'content', __('Content', 'rde') )
//                        ->set_attribute('maxLength', '150')
                        ->set_required()
                ) )
                ->set_header_template( '<% if (title) { %><%- title %><% } %>' )
        ) );
    }

    function front_end($args, $instance){
        if( isset($instance['steps']) && is_array($instance['steps']) && !empty($instance['steps']) ) {
            echo '<div class="wrapperLimited">'
                . '<div class="wrapper">'
                . '<div class="section fourColumns">'
                . '<div class="sectionHeadline">'
                . '<h3>' . $instance['title'] . '</h3>'
                . '</div>'
                . '<div class="row">';

            foreach( $instance['steps'] as $key=>$step ){
                echo '<div class="col-sm-6 col-md-3">'
                        .'<div class="column">'
                            .'<div class="number">'.($key+1).'</div>'
                            .'<div class="title">'.$step['title'].'</div>'
                            .'<div class="desc">'.$step['content'].'</div>'
                        .'</div>'
                    .'</div>';
            }

            echo '</div></div></div></div>';
        }
    }
}

function load_widgets() {
    register_sidebar(
        array(
            'name' => __( 'Bottom sidebar', 'rde' ),
            'id' => 'bottom-sidebar',
            'description' => __( 'Sidebar on bottom', 'rde' ),
            'before_widget' => '',
            'after_widget'  => ''
        )
    );

    register_widget( 'ClientsWidget' );
    register_widget( 'StepsWidget' );
}