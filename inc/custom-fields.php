<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
// front-page.php custom fields
    Container::make( 'post_meta', __( 'Front page meta data', 'rde' ) )
        ->where( 'post_type', '=', 'page' ) // only show our new fields on pages
        ->where( 'post_template', '=', 'front-page.php' )
        ->add_fields( array(
            Field::make( 'image', 'video', __('Video', 'rde') )
                ->set_type( 'video' ),
            Field::make( 'complex', 'slides', __('Slides', 'rde') )
                ->set_layout( 'tabbed-vertical' )
                ->add_fields( array(
                    Field::make( 'text', 'title', __('Title', 'rde') )
                        ->set_required(),
                    Field::make( 'textarea', 'text', __('Text','rde') ),
                    Field::make( 'complex', 'button' )
                        ->set_layout( 'tabbed-vertical' )
                        ->add_fields( 'simple', array(
                            Field::make( 'text', 'button_text', __('Button text','rde') )
                                ->set_default_value( __('Show products','rde') )
                                ->set_required(),
                            Field::make( 'text', 'link', __('Button link','rde') )
                                ->set_required(),
                        ) )
                        ->set_header_template( '<% if (button_text) { %><%- button_text %><% } %>' )
                        ->add_fields('oembed', array(
                            Field::make( 'text', 'button_text', __('Button name','rde') )
                                ->set_default_value( __('Show video','rde') )
                                ->set_required(),
                            Field::make( 'oembed', 'uri', __('Video Uri','rde') )
                                ->set_required(),
                        ) )
                        ->set_header_template( '<% if (button_text) { %><%- button_text %><% } %>' )
                ) )
                ->set_header_template( '<% if (title) { %><%- title %><% } %>' )
        ) );

// portfolio.php custom fields
    Container::make( 'post_meta', __( 'Portfolio page meta', 'rde' ) )
        ->where( 'post_type', '=', 'page' ) // only show our new fields on pages
        ->where( 'post_template', '=', 'portfolio.php' )
        ->add_fields( array(
            Field::make( 'text', 'posts_per_page', __('Posts per page', 'rde') )
                ->set_default_value( '15' )
                ->set_attribute( 'type', 'number' )
                ->set_required(),
        ) );

// about-us.php custom fields
    Container::make( 'post_meta', __( 'About us page meta', 'rde' ) )
        ->where( 'post_type', '=', 'page' )
        ->where( 'post_template', '=', 'about-us.php' )
        ->add_fields( array(
            Field::make( 'complex', 'columns', __('Columns content', 'rde') )
                ->add_fields( array(
                    Field::make( 'rich_text', 'content', __('Content', 'rde') )
                        ->set_required()
                ) )
//                ->set_collapsed(true)
        ) );

//  single_post custom fields
    Container::make( 'post_meta', __( 'Page meta data', 'rde' ) )
        ->where( 'post_type', '=', 'post' ) // only show our new fields on pages
        ->add_fields( array(
            Field::make( 'complex', 'post_slides', __('Slides', 'rde') )
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( 'image', array(
                    Field::make( 'image', 'photo', __('Photo', 'rde') )
                ) )
                ->add_fields('oembed', array(
                    Field::make( 'oembed', 'uri', __('Video Uri', 'rde') )
                ) ),
        ) );

//  Single portfolio custom fields
    Container::make( 'post_meta', __( 'Page meta teaser', 'rde' ) )
        ->where( 'post_type', '=', 'portfolio' ) // only show our new fields on pages
        ->add_fields( array(
            Field::make( 'select', 'teaser_view', __('Teaser view', 'rde') )
                ->add_options( array(
                    'sizeX1' => __('Normal', 'rde'),
                    'sizeX2' => __('Size 2X', 'rde')
                ) )
                ->set_default_value('sizeX1'),
            Field::make( 'image', 'teaser_cover_image', __('Teaser cover image', 'rde') )
                ->set_required()
        ) );

    Container::make( 'post_meta', __( 'Page meta data', 'rde' ) )
        ->where( 'post_type', '=', 'portfolio' ) // only show our new fields on pages
        ->add_fields( array(
            Field::make( 'text', 'copyright', __('Copyright', 'rde') ),
            Field::make( 'text', 'client', __('Client', 'rde') ),
            Field::make( 'text', 'client_url', __('Client url', 'rde') ),
            Field::make( 'text', 'gallery_title', __('Gallery title', 'rde') )
                ->set_default_value('Gallery'),
            Field::make( 'complex', 'gallery', __('Gallery', 'rde') )
                ->set_layout( 'tabbed-vertical' )
                ->add_fields( array(
                    Field::make( 'image', 'image', __('Image', 'rde') )
                        ->set_required()
                ) )
        ) );

//  theme options custom fields
    Container::make( 'theme_options', __( 'Theme Options', 'rde' ) )
        ->add_fields( array(
            Field::make( 'complex', 'crb_complex', 'My Complex Field' )
                ->add_fields( array(
                    Field::make( 'text', 'my_text_field', 'My Text Field' )
                ) )
                ->set_default_value( array(
                    array(
                        'my_text_field' => 'Hello',
                    ),
                    array(
                        'my_text_field' => 'World!',
                    ),
                ) ),

        ) );
//    Container::make( 'post_meta', __('User Settings') )
//        ->where( 'post_type', '=', 'page' )
//        ->add_tab( __('Profile'), array(
//            Field::make( 'text', 'crb_first_name', 'First Name' ),
//            Field::make( 'text', 'crb_last_name', 'Last Name' ),
//            Field::make( 'text', 'crb_position', 'Position' ),
//        ) )
//        ->add_tab( __('Notification'), array(
//            Field::make( 'text', 'crb_email', 'Notification Email' ),
//            Field::make( 'text', 'crb_phone', 'Phone Number' ),
// ) );

//  Product categories custom fields
    Container::make( 'term_meta', __( 'Template view', 'rde' ) )
        ->where( 'term_taxonomy', '=', 'product_cat' ) // only show our new field for categories
        ->add_fields( array(
            Field::make( 'checkbox', 'hide', __('Hide from display', 'rde')  ),
            Field::make( 'select', 'type', __('View type', 'rde') )
                ->add_options( array(
                    'simple' => __('Simple','rde'),
                    'b2b' => __('B2B Table','rde'),
                    'deejays' => __('Deejays','rde')
                ) )
                ->set_default_value( 'simple' )
        ) );
//  Product custom metas
    Container::make( 'post_meta', __( 'Product specifications', 'rde' ) )
        ->where( 'post_type', '=', 'product' )
        ->where( 'post_term', 'CUSTOM', function( $post_id ){
            $terms = wp_get_post_terms( $post_id, 'product_cat', array('fields'=>'ids') );
            if( carbon_get_term_meta( $terms[0], 'type' ) == 'b2b' )
                return true;
            return false;
        } )
        ->add_fields( array(
            Field::make( 'complex', 'specs', __('Specifications', 'rde') )
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'text', 'title', __('Tab title', 'rde') )
                        ->set_required(),
                    Field::make( 'rich_text', 'content', __('Tab content', 'rde') )
                        ->set_required()
                ) )
//                ->set_default_value( array(
//                    array(
//                        'title' => __('Specification', 'rde')
//                    ),
//                    array(
//                        'title' => __('Options', 'rde')
//                    ),
//                    array(
//                        'title' => __('Use', 'rde')
//                    )
//                ) )
                ->set_header_template( '<% if (title) { %><%- title %><% } %>' ),
            Field::make( 'complex', 'video_section', __('Video section', 'rde') )
                ->set_max(1)
                ->add_fields( array(
                    Field::make( 'text', 'title', __('Title', 'rde') )
                        ->set_required()
                        ->set_default_value( __('Completely new solution for DJ Festivals', 'rde') ),
                    Field::make( 'text', 'sub_title', __('Subtitle', 'rde') )
                        ->set_required()
                        ->set_default_value( __('Rent a product for your festival with professional DJ equipment', 'rde') ),
                    Field::make( 'oembed', 'video', __('Video', 'rde') )
                        ->set_required()
                ) )
        ) );

    Container::make( 'post_meta', __( 'Product specifications', 'rde' ) )
        ->where( 'post_type', '=', 'product' )
        ->where( 'post_term', 'CUSTOM', function( $post_id ){
            $terms = wp_get_post_terms( $post_id, 'product_cat', array('fields'=>'ids') );
            if( carbon_get_term_meta( $terms[0], 'type' ) == 'deejays' )
                return true;
            return false;
        } )
        ->add_fields( array(
            Field::make( 'complex', 'socials', __('Socials', 'rde') )
                ->add_fields( 'facebook', array(
                    Field::make( 'text', 'url', __('Url', 'rde') )
                        ->set_required()
                ) )
                ->add_fields( 'soundcloud', array(
                    Field::make( 'text', 'url', __('Url', 'rde') )
                        ->set_required()
                ) ),
            Field::make( 'complex', 'music', __('Music section', 'rde') )
                ->add_fields( 'soundcloud', array(
                    Field::make( 'text', 'url', __('Iframe url', 'rde') )
                        ->set_required()
                ) )
        ) );

// contact.php custom fields
    Container::make( 'post_meta', __( 'Contact meta data', 'rde' ) )
        ->where( 'post_type', '=', 'page' ) // only show our new fields on pages
        ->where( 'post_template', '=', 'contact.php' )
        ->add_fields( array(
            Field::make( 'complex', 'addresses', __('Addresses', 'rde') )
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'text', 'title', __('Title', 'rde') )
                        ->set_required(),
                    Field::make( 'rich_text', 'text', __('Text','rde') )
                        ->set_required()
                ) )
                ->set_header_template( '<% if (title) { %><%- title %><% } %>' ),
            Field::make( 'complex', 'contacts', __('Addresses', 'rde') )
                ->set_layout( 'tabbed-horizontal' )
                ->add_fields( array(
                    Field::make( 'image', 'thumb', __('Image', 'rde') )
                        ->set_required(),
                    Field::make( 'text', 'title', __('Title', 'rde') )
                        ->set_required(),
                    Field::make( 'text', 'company_title', __('Company title', 'rde') )
                        ->set_default_value('Crew'),
                    Field::make( 'rich_text', 'text', __('Text','rde') )
                        ->set_required(),
                    Field::make( 'text', 'class', __('Class','rde') )
                ) )
                ->set_header_template( '<% if (title) { %><%- title %><% } %>' )
        ) );
}