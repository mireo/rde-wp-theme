<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<div class="grid-item">
    <div class="itemContainer">
        <div class="itemNews">
            <div class="thumb">
                <a href="<?php the_permalink();?>" style="background-image: url('<?php echo get_the_post_thumbnail_url( $post, 'thumbnail' );?>');"></a>
            </div>
            <div class="desc">
                <div class="title"><a href="<?php the_permalink();?>"><?php the_title();?></a></div>
                <div class="date"><span class="fa fa-clock-o"></span> <?php the_time(get_option('date_format'));?></div>
            </div>
        </div>
    </div>
</div>