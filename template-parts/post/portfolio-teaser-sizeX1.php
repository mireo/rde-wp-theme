<?php
$logo = carbon_get_the_post_meta('teaser_cover_image');
$thumb = carbon_get_the_post_meta('gallery');
if( $thumb && isset($thumb[0]))
    $thumb = wp_get_attachment_image_src( $thumb[0]['image'], 'thumbnail' );

?>
<div class="grid-item">
    <div class="itemContainer itemPortfolio">
        <div class="itemNews">
            <a href="<?php the_permalink();?>" class="link"></a>
            <div class="zoom"><span class="fa fa-search"></span></div>
            <div class="logo">
                <div class="logoContainer"><?php echo wp_get_attachment_image($logo, 'logo-h90');?></div>
            </div>
            <div class="thumb">
                <a style="background-image: url(<?php echo $thumb[0];?>)"></a>
            </div>
        </div>
    </div>
</div>
<!--
<div class="grid-item sizeX2">
    <div class="itemContainer itemPortfolio">
        <a href="#" class="link"></a>
        <div class="zoom"><span class="fa fa-search"></span></div>
        <div class="logo">
            <div class="logoContainer"><img src="images/logos/1.jpg" alt=""></div>
        </div>
        <div class="itemPortfolioSlider">
            <ul>
                <li>
                    <div class="thumb">
                        <a style="background-image: url(images/temp/thumbNews2.jpg)"></a>
                    </div>
                </li>
                <li>
                    <div class="thumb">
                        <a style="background-image: url(images/temp/thumbNews3.jpg)"></a>
                    </div>
                </li>
                <li>
                    <div class="thumb">
                        <a style="background-image: url(images/temp/thumbNews1.jpg)"></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
-->