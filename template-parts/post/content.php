
<div class="section product">
    <div class="row">
        <div class="col-lg-6">
            <div class="columnLeft">
                <div class="title"><h1><?php the_title();?></h1></div>
                <div class="date"><span class="fa fa-clock-o"></span> <?php the_time(get_option('date_format'));?></div>
                <div class="description">
                    <?php the_content();?>
                </div>
            </div>
        </div>
        <?php
            $post_slides = carbon_get_the_post_meta( 'post_slides' );
            if( $post_slides ):
        ?>
        <div class="col-lg-6">
            <div class="productMainGallery">
                <ul>
                    <?php foreach( $post_slides as $key=>$value ):?>
                    <li>
                        <div class="item">
                            <div class="itemContainer">
                                <?php switch( $value['_type'] ){
                                    case 'image':
                                        $src = '';
                                        echo wp_get_attachment_image($value['photo'], 'medium');
                                    break;
                                    case 'oembed':
//                                        <!--oembed-->
                                        echo wp_oembed_get( $value['uri'], array() );
                                    break;
                                    default:?>
                                        <!--default-->
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>