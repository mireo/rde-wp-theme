<?php
$logo = carbon_get_the_post_meta('teaser_cover_image');
$thumbs = carbon_get_the_post_meta('gallery');
?>

<div class="grid-item sizeX2">
    <div class="itemContainer itemPortfolio">
        <a href="<?php the_permalink();?>" class="link"></a>
        <div class="zoom"><span class="fa fa-search"></span></div>
        <div class="logo">
            <div class="logoContainer"><?php echo wp_get_attachment_image($logo, 'logo-h90');?></div>
        </div>
        <div class="itemPortfolioSlider">
            <ul>
                <?php foreach( $thumbs as $key=>$thumb ):?>
                    <?php
                        $image = wp_get_attachment_image_src( $thumb['image'], 'medium' );
                    ?>
                <li>
                    <div class="thumb">
                        <a style="background-image: url(<?php echo $image[0];?>)"></a>
                    </div>
                </li>
                    <?php
                    if( $key > 2 )
                        break;
                    ?>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>
