
<div class="section product">
    <div class="row">
        <div class="col">
            <div class="columnLeft">
                <div class="title"><h1><?php the_title();?></h1></div>
                <div class="date"><span class="fa fa-clock-o"></span> <?php the_time(get_option('date_format'));?></div>
            </div>
        </div>
    </div>
    <div class="row portfolioOpen">
        <div class="col-md-6">
            <div class="columnLeft">
                <div class="description">
                    <?php the_content();?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table>
                <tbody>
                <?php if( $terms = wp_get_post_terms(get_the_ID(), 'portfolio_category', array("fields" => "names") ) ):?>
                <tr>
                    <td><?php _e('Categories','rde');?>:</td>
                    <td><?php echo join(', ', $terms);?></td>
                </tr>
                <?php endif;?>
                <?php if( $terms = wp_get_post_terms(get_the_ID(), 'portfolio_tag', array("fields" => "names") ) ):?>
                <tr>
                    <td><?php _e('Skills Needed','rde');?>:</td>
                    <td><?php echo join(', ', $terms);?></td>
                </tr>
                <?php endif;?>
                <?php if( $terms = wp_get_post_terms(get_the_ID(), 'portfolio_date', array("fields" => "names") ) ):?>
                <tr>
                    <td><?php _e('Date','rde');?>:</td>
                    <td><?php echo join(', ', $terms);?></td>
                </tr>
                <?php endif;?>
                <?php if( $value = carbon_get_the_post_meta( 'copyright' ) ):?>
                <tr>
                    <td><?php _e('Copyright','rde');?>:</td>
                    <td><?php echo $value;?></td>
                </tr>
                <?php endif;?>
                <?php if( $value = carbon_get_the_post_meta( 'client' ) ):?>
                <tr>
                    <td><?php _e('Client','rde');?>:</td>
                    <td><?php echo $value;?></td>
                </tr>
                <?php endif;?>
                <?php if( $value = carbon_get_the_post_meta( 'client_url' ) ):?>
                <tr>
                    <td><?php _e('Client url','rde');?>:</td>
                    <td><a href="$<?php echo $value;?>"><?php echo $value;?></a></td>
                </tr>
                <?php endif;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
$gallery = carbon_get_the_post_meta('gallery');
if( $gallery ):?>
<div class="section relatedProducts">
    <div class="sectionHeadline">
        <div class="sectionHeadlineRow">
            <div class="columnLeft">
                <h3><?php echo carbon_get_the_post_meta('gallery_title');?></h3>
            </div>
        </div>
    </div>
    <div class="itemsGrid">
        <div class="grid-sizer"></div>
        <?php foreach( $gallery as $image ):?>
        <?php
            $thumb = wp_get_attachment_image_src( $image['image'], 'thumbnail' );
            $photo = wp_get_attachment_image_src( $image['image'], 'large' );
        ?>
        <div class="grid-item">
            <div class="itemContainer itemGallery">
                <div class="thumb">
                    <a class="fancybox-effects-d" data-fancybox-group="gallery" href="<?php echo $photo[0];?>" style="background-image: url(<?php echo $thumb[0];?>)"></a>
                    <div class="zoom"><span class="fa fa-search"></span></div>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>
<?php endif;?>