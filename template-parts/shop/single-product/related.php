<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( $related_products ) : ?>
<div class="section relatedProducts">
    <div class="sectionHeadline">
        <div class="sectionHeadlineRow">
            <div class="columnLeft">
                <h3><?php echo (isset($args['header']))?$args['header']:__( 'Related products', 'woocommerce' ); ?></h3>
            </div>
        </div>
    </div>
    <div class="itemsGrid">
        <div class="grid-sizer"></div>
        <?php foreach ( $related_products as $related_product ) : ?>

            <?php
            $post_object = get_post( $related_product->get_id() );

            setup_postdata( $GLOBALS['post'] =& $post_object );

            $type = '';

            if( $terms = get_the_terms( get_the_ID(), 'product_cat' ) ){
                if( 'deejays' == carbon_get_term_meta( $terms[0]->term_id, 'type' ) )
                    $type = '-deejays';
            }
            wc_get_template_part( 'product'.$type, 'teaser' ); ?>

        <?php endforeach; ?>
        <?php wp_reset_postdata();?>
    </div>
</div>
<?php endif;?>