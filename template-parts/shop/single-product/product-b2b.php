<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

global $product;

?>
<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <div class="section product">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="columnLeft">
                            <?php woocommerce_simple_add_to_cart();?>
                            <div class="title"><h1><?php the_title();?></h1></div>
                            <div class="category"><?php echo get_the_term_list( get_the_ID(), 'product_cat', '', ', ' ); ?></div>
                            <div class="description">
                                <?php the_content();?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="productMainGallery">
                            <ul>
                                <?php
                                $attachment_ids = $product->get_gallery_image_ids();

                                if( $attachment_ids ){ foreach( $attachment_ids as $attachment_id ):
                                    ?>
                                    <li>
                                        <div class="item">
                                            <div class="itemContainer">
                                                <?php echo wp_get_attachment_image($attachment_id, 'shop_single');?>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach;?>
                                <?php }else{ ?>
                                    <li>
                                        <div class="item">
                                            <div class="itemContainer">
                                                <?php the_post_thumbnail('shop_single');?>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php if( $tabs = carbon_get_the_post_meta('specs') ):?>
            <div class="section tabs">
                <div class="tabsHeader">
                    <?php foreach( $tabs as $key=>$tab ):?>
                    <div class="button button<?php echo $key+1;echo ($key==0)?' current':'';?>"><a><?php echo $tab['title'];?><span></span></a></div>
                    <?php endforeach;?>
                </div>
                <div class="tabsContent">
                    <?php foreach( $tabs as $key=>$tab ):?>
                    <div class="tab tabContent<?php echo $key+1;echo ($key==0)?' current':'';?>">
                        <?php echo apply_filters('the_content', $tab['content']);?>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
            <?php endif;?>
        </div>
    </div>
</div>
<?php if( $video = carbon_get_the_post_meta('video_section') ):?>
<div class="videoSection">
    <div class="mask"></div>
    <div class="gradientMask"></div>
    <div class="videoSectionTable">
        <div class="videoSectionColumn">
            <div class="wrapperLimited">
                <div class="wrapper">
                    <h2><?php echo $video[0]['title'];?></h2>
                    <h3><?php echo $video[0]['sub_title'];?></h3>
                    <div class="buttons">
                        <a class="showVideo" href="<?php echo $video[0]['video'];?>"  data-lity><span class="fa fa-play"></span> <?php _e('Show full video', 'rde');?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <?php woocommerce_related_products();?>
        </div>
    </div>
</div>

