<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}

$type = '';
if( $terms = get_the_terms( get_the_ID(), 'product_cat' ) ){
    $type = carbon_get_term_meta( $terms[0]->term_id, 'type' );
}

if( $type ) {
    wc_get_template_part('single-product/product', $type);
}else{
?>
<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <div class="section product">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="columnLeft">
                            <?php woocommerce_simple_add_to_cart();?>
                            <div class="title"><h1><?php the_title();?></h1></div>
                            <div class="category"><?php echo get_the_term_list( get_the_ID(), 'product_cat', '', ', ' ); ?></div>
                            <div class="description">
                                <?php the_content();?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="productMainGallery">
                            <ul>
                                <?php
                                $attachment_ids = $product->get_gallery_image_ids();

                                if( $attachment_ids ){ foreach( $attachment_ids as $attachment_id ):
                                ?>
                                <li>
                                    <div class="item">
                                        <div class="itemContainer">
                                            <?php echo wp_get_attachment_image($attachment_id, 'shop_single');?>
                                        </div>
                                    </div>
                                </li>
                                <?php endforeach;?>
                                <?php }else{ ?>
                                <li>
                                    <div class="item">
                                        <div class="itemContainer">
                                            <?php the_post_thumbnail('shop_single');?>
                                        </div>
                                    </div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php woocommerce_related_products();?>
        </div>
    </div>
</div>
<?php }?>
