<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}


?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

        <div class="sectionHeadline">
            <h3><?php _e('Fill the form', 'rde');?></h3>
        </div>
        <div class="cartForm">
            <div class="row">
                <?php
                $fields = $checkout->get_checkout_fields( 'billing' );

                foreach ( $fields as $key => $field ):
                    if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
                        $field['country'] = $checkout->get_value( $field['country_field'] );
                    }
                ?>
                <div class="col-md-6">
                    <div class="inputContainer">
                        <?php if( $key != 'billing_transport' )woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );?>
                    </div>
                </div>
                <?php endforeach;?>
                <div class="col-sm-12">
                    <div class="inputContainer">
                        <?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) : ?>
                            <?php $filed['placeholder'] = __('Message', 'rde');
                            unset( $field['label'] );
                                woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="confirm">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php
                        $field = array(
                            'id' => 'transport',
                            'label'=> __('Add transport service', 'rde'),
                            'autocomplete' => 'given-name',
                            'autofocus' => 1,
                            'type' => 'checkbox'
                        );
                        rde_form_field( 'billing_transport', $field, 0 ); ?>
<!--                        <input type="checkbox" id="transport" name="transport" value="1">-->
<!--                        <label for="transport"><span></span>Add transport service</label>-->
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="button">
                    <?php
                    $order_button_text = __('Send a question','rde');
                    echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">'.$order_button_text.'</button>' ); ?>
                    <?php wp_nonce_field( 'woocommerce-process_checkout' ); ?>
                    </div>
                </div>
            </div>
        </div>


<!--				--><?php //do_action( 'woocommerce_checkout_billing' ); ?>

				<?php //do_action( 'woocommerce_checkout_shipping' ); ?>


<!--		--><?php //do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>


	<?php //do_action( 'woocommerce_checkout_before_order_review' ); ?>

		<?php //do_action( 'woocommerce_checkout_order_review' ); ?>

	<?php //do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
