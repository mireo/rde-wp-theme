<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$terms = get_the_terms( get_the_ID(), 'product_cat' );

$term_slugs = array();
if( $terms ) foreach( $terms as $term )
    $term_slugs[] = $term->slug;

?>

<div class="grid-item <?php echo implode(' ', $term_slugs);?>">
    <div class="itemContainer">
        <div class="itemProduct">
            <?php if( $product->get_featured() ):?><div class="labelNew"><?php _e('New','rde');?></div><?php endif;?>
            <?php
                woocommerce_template_loop_add_to_cart( array(
                    'quantity' => 1
                ) );
            ?>
            <div class="productThumb">
                <div class="productThumbContainer">
                    <a href="<?php the_permalink();?>"><?php the_post_thumbnail('shop_catalog');?></a>
                </div>
            </div>
            <div class="category"><?php echo ($terms && isset($terms[0]))?$terms[0]->name:'';?></div>
            <div class="title"><a href="<?php the_permalink();?>"><?php the_title();?></a></div>
        </div>
    </div>
</div>
