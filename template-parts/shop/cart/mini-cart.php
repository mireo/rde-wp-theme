<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

do_action( 'woocommerce_before_mini_cart' ); ?>
<div class="widget_shopping_cart_content">
    <div class="cart">
        <a href="<?php echo wc_get_cart_url();?>" class="<?php echo WC()->cart->is_empty()?'':'active';?>"> <!-- class active if value cart is more than 1 -->
            <span class="fa fa-shopping-cart"></span>

            <span class="value"><?php echo WC()->cart->get_cart_contents_count(); ?></span>

        </a>
    </div>
</div>