<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//wc_print_notices();

/**
 * @hooked wc_empty_cart_message - 10
 */
?>
<div class=''>
    <?php
    $args = array(
        'posts_per_page' => 4,
        'post_type' => 'product'
    );
    $latest_posts = new WP_Query( $args );
    if( $latest_posts->have_posts() ):?>
        <div class="section latestProducts">
            <div class="sectionHeadline">
                <div class="sectionHeadlineRow">
                    <div class="columnLeft">
                        <h3><?php _e('Latest products', 'rde');?></h3>
                    </div>
                    <div class="columnRight">
                        <div class="showAll">
                            <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) );?>"><?php _e('Show All', 'rde');?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="itemsGrid">
                <div class="grid-sizer"></div>
                <?php while ( $latest_posts->have_posts() ) : $latest_posts->the_post(); ?>
                    <?php $type = '';?>
                    <?php
                    if( $terms = get_the_terms( get_the_ID(), 'product_cat' ) ){
                        if( 'deejays' == carbon_get_term_meta( $terms[0]->term_id, 'type' ) )
                            $type = '-deejays';
                    }
                    ?>
                    <?php wc_get_template_part( 'product'.$type, 'teaser' ); ?>

                <?php endwhile; wp_reset_postdata();?>
            </div>
        </div>
    <?php endif;?>

</div>
