<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

//wc_print_notices();
?>

<?php do_action( 'woocommerce_before_cart' ); ?>
<div class="section sectionCart">
    <div class="row">
        <div class="col-xl-6">
            <form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
                <?php do_action( 'woocommerce_before_cart_table' ); ?>
                <div class="woocommerce-cart-form__contents">
                <?php foreach ( apply_filters('rde_group_products', WC()->cart->get_cart()) as $group): ?>
                <div class="sectionHeadline">
                    <h3><?php echo $group['name'];?></h3>
                    <?php //echo print_r($cart_it['data'],1);exit;?>
                </div>
                <div class="cartProducts">
                    <ul>
                        <?php
                        foreach( $group['items'] as $cart_item_key => $cart_item ):
                        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
                        $terms = get_the_terms( $product_id, 'product_cat' );


                        $dj = false;
                        if( $cats = get_the_terms( $product_id, 'product_cat' ) ){
                            if( 'deejays' == carbon_get_term_meta( $cats[0]->term_id, 'type' ) )
                                $dj = true;
                        }

                        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) :
                            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                        ?>
                        <li>
                            <div class="item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                                <div class="thumb">
                                    <?php
                                    if( !$dj ) {
                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                        if (!$product_permalink) {
                                            echo $thumbnail;
                                        } else {
                                            printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail);
                                        }
                                    }else{
                                        $thumbnail = get_the_post_thumbnail_url( $product_id, 'shop_thumbnail' );
                                        printf('<a class="djThumb" href="%s" style="background-image:url(%s)"></a>', esc_url($product_permalink), $thumbnail);
                                    }
                                    ?>
                                </div>
                                <div class="desc">
                                    <div class="category"><?php echo ($terms && isset($terms[0]))?$terms[0]->name:'';?></div>
                                    <div class="title">
                                        <?php echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );?>
                                    </div>
                                </div>
                                <div class="options">
                                    <?php if(!$dj): ?>
                                    <?php
                                    if ( $_product->is_sold_individually() ) {
                                        $product_quantity = sprintf( '1 <input type="text" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                    } else {
                                        $product_quantity = woocommerce_quantity_input( array(
                                            'input_name'  => "cart[{$cart_item_key}][qty]",
                                            'input_value' => $cart_item['quantity'],
                                            'max_value'   => $_product->get_max_purchase_quantity(),
                                            'min_value'   => '0',
                                        ), $_product, false );
                                    }
                                    echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                                    ?>
                                    <?php endif;?>
                                    <?php
                                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                        '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s"><span class="fa fa-close"></span></a>',
                                        esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                        __( 'Remove this item', 'woocommerce' ),
                                        esc_attr( $product_id ),
                                        esc_attr( $_product->get_sku() )
                                    ), $cart_item_key );
                                    ?>
                                </div>
                            </div>
                        </li>
                        <?php endif; endforeach;?>
                    </ul>
                </div>
                <?php endforeach;?>
                <input type="submit" style="display:none;" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>" />
                <?php do_action( 'woocommerce_cart_actions' ); ?>
                <?php wp_nonce_field( 'woocommerce-cart' ); ?>

                </div>
                <?php do_action( 'woocommerce_after_cart_table' ); ?>
            </form>
        </div>
        <div class="col-xl-6">
            <?php WC_Shortcode_Checkout::output(array());?>
        </div>
    </div>
</div>

<div class="cart-collaterals">
    <?php
    /**
     * woocommerce_cart_collaterals hook.
     *
     * @hooked woocommerce_cross_sell_display
     * @hooked woocommerce_cart_totals - 10
     */
    //do_action( 'woocommerce_cart_collaterals' );
    ?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>