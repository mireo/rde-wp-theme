<?php
/* Template Name: 404 Page */

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>



<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <?php echo get_template_part( 'head' );?>
</head>

<body <?php body_class(); ?>>

<?php get_header(); ?>
<!--
<div class="mainContent subpage">
    <div class="wrapperLimited">
        <div class="wrapper">
            <?php
            if( have_posts() ) {
                while ( have_posts() ) {
                    the_post();

            ?>
            <div class="videoSection" style="height: 100%;">
                <div class="mask"></div>
                <div class="gradientMask"></div>
                <div class="videoSectionTable" style="height: 100%;">
                    <div class="videoSectionColumn">
                        <div class="wrapperLimited">
                            <div class="wrapper">
                                <h2>Błąd 404</h2>
                                <h3>Przepraszamy, strona której szukasz, nie została odnaleziona</h3>
                                <div class="buttons">
                                    <a class="showVideo" href="#">Powrót na stronę główną</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                    the_content();
                }
                wp_reset_postdata();
            }
            ?>
        </div>
    </div>
</div>-->

<?php
$page = get_posts(array(
    'posts_per_page' => 1,
    'post_type' => 'page',
    'meta_key' => '_wp_page_template',
    'meta_value' => '404.php'
));
if( $page ):
?>
<div class="videoSection" style="height: 100%;">
    <div class="mask"></div>
    <div class="gradientMask"></div>
    <div class="videoSectionTable" style="height: 100%;">
        <div class="videoSectionColumn">
            <div class="wrapperLimited">
                <div class="wrapper">
                    <h2><?php echo $page[0]->post_title;?></h2>
                    <?php echo apply_filters('the_content', $page[0]->post_content);?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;?>
<?php //get_footer();?>
</body>
</html>

