<?php
require_once( 'inc/carbon-fields/vendor/autoload.php' );
\Carbon_Fields\Carbon_Fields::boot();
require 'inc/custom-fields.php';
require 'inc/custom-widgets.php';
require 'inc/custom-post_types.php';
require 'inc/taxonomy-single-term/class.taxonomy-single-term.php';
//flush_rewrite_rules( false );

//$product_cat = new Taxonomy_Single_Term('product_cat');


function custom_locate_template($template, $template_name, $template_path){
    $temp = locate_template(
        array(
            trailingslashit( 'template-parts/shop/' ) . $template_name,
            $template_name,
        )
    );
    if ( file_exists( $temp ) ) {
        $template = $temp;
    }

    return $template;
}

add_filter( 'woocommerce_locate_template', 'custom_locate_template', 100, 100, 100 );

function rde_get_template_part( $template, $slug, $name ){

    $temp = join('',
        array(
            get_template_directory(),
            trailingslashit( '/template-parts/shop/' ) . $slug,
            '-',
            $name,
            '.php'
        )
    );
    if ( file_exists( $temp ) ) {
        $template = $temp;
    }

    return $template;
}
add_filter( 'wc_get_template_part', 'rde_get_template_part', 100, 100, 100);

function rde_group_products( $products ){
    $groups = array();
    foreach( $products as $product ){
        $terms = get_the_terms( $product['product_id'], 'product_cat' );
        if( $terms && is_array($terms) ){
            $groups[$terms[0]->term_id]['name'] = $terms[0]->name;
            $groups[$terms[0]->term_id]['items'][$product['key']] = $product;
        }else{
            if( !isset($groups['other']) )
                $groups['other']['name'] = __('Other', 'rde');
            $groups['other']['items'][$product['key']] = $product;
        }
    }
    if( $groups )
        $sort = get_terms('product_cat');
    if( $sort ) {
        $h = array();
        foreach( $sort as $s ){
            if( isset( $groups[$s->term_id] ) )
                $h[] = $groups[$s->term_id];
        }
        if( isset( $groups['other'] ) )
            $h[ count($h) ] = $groups['other'];
        $groups = $h;
        unset( $sort );
        unset( $h );
    }
    return $groups;
}

add_filter( 'rde_group_products', 'rde_group_products', 1 );

function product_in_cart($product_id) {
    if( is_object($product_id) )
        $product_id = $product_id->get_id();
    foreach(WC()->cart->get_cart() as $key => $_product ) {

        if( $product_id == $_product['product_id'] ) {
            return true;
        }
    }

    return false;
}

function mode_theme_update_mini_cart() {
    echo WC()->cart->get_cart_contents_count();
    die();
}

add_filter( 'wp_ajax_nopriv_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );
add_filter( 'wp_ajax_mode_theme_update_mini_cart', 'mode_theme_update_mini_cart' );

function get_terms_of_displayed_posts($tax = 'category', $number = 10){
    global $posts;

    $uniq_terms = array();
    if ( have_posts() ) foreach( $posts as $post ){
        $terms = get_the_terms( $post->ID, $tax);
        if( $terms ) foreach($terms as $term) {
            if( is_tax() ) {
                if($term->parent && !carbon_get_term_meta( $term->term_id, 'hide' ))
                    $uniq_terms[$term->term_id] = $term;
            }else {
                if( !carbon_get_term_meta( $term->term_id, 'hide' ) )
                    $uniq_terms[$term->term_id] = $term;
            }
            if( count($uniq_terms) >= $number )
                break;
        }
        if( count($uniq_terms) >=$number )
            break;
    }
    if( $uniq_terms ) {
        $sort_uniq = array();
        $terms_order = get_terms(array(
            'taxonomy' => $tax
//            'hide_empty' => true
        ));

        foreach ($terms_order as $term){
            if( array_key_exists( $term->term_id, $uniq_terms ) )
                $sort_uniq[] = $term;
        }
        $uniq_terms = $sort_uniq;
    }

//    echo print_r( $uniq_terms,1);exit;

    return $uniq_terms;
}

add_filter( 'woocommerce_add_to_cart_validation', 'bbloomer_only_one_in_cart', 99, 2 );

function bbloomer_only_one_in_cart( $passed, $added_product_id ) {
    if( product_in_cart($added_product_id) )
        $passed = false;

//    wc_add_notice( 'Product is in cart!', 'rde' );

    return $passed;
}



add_action( 'admin_notices', 'rde_theme_dependencies' );

function rde_theme_dependencies() {
    if( ! class_exists('Polylang') )
        echo '<div class="error"><p>' . __( 'Warning: The theme needs plugin Polylang to function', 'rde' ) . '</p></div>';
    if( ! function_exists('wc') )
        echo '<div class="error"><p>' . __( 'Warning: The theme needs plugin Woocommerce to shop function', 'rde' ) . '</p></div>';
}

function rde_checkout_fields($fields){
    if( isset($fields['billing']) && true ){
        $afields = array(
            'billing_name' => array( 'placeholder' => __('Full name', 'rde'), 'label'=> __('Full name', 'rde'), 'required' => 1, 'autocomplete' => 'given-name', 'autofocus' => 1 ),
            'billing_company' => array( 'placeholder' => __('Company name', 'rde'), 'label'=> __('Company name', 'rde'), 'required' => 0 ),
            'email' => array( 'placeholder' => __('E-mail', 'rde'), 'label'=> __('E-mail', 'rde'), 'required' => 1, 'validate' => array('email') ),
            'billing_phone' => array( 'placeholder' => __('Phone', 'rde'), 'label'=> __('Phone', 'rde'), 'validate' => array('phone'), 'autocomplete' => 'tel' ),
            'billing_event_venue' => array( 'placeholder' => __('Event venue', 'rde'), 'label'=> __('Event venue', 'rde'), 'required' => 0, 'autocomplete' => 'address-line1' ),
            'billing_event_name' => array( 'placeholder' => __('Event name', 'rde'), 'label'=> __('Event name', 'rde'), 'required' => 0 ),
            'billing_date_start' => array( 'placeholder' => __('Date of recepit', 'rde'), 'label'=> __('Date of recepit', 'rde'), 'required' => 0 ),
            'billing_date_end' => array( 'placeholder' => __('Date of return', 'rde'), 'label'=> __('Date of return', 'rde'), 'required' => 0 ),
            'billing_transport' => array( 'required' => 0 )
        );
        $fields['billing'] = $afields;

    }
    return $fields;
}

add_filter('woocommerce_checkout_fields', 'rde_checkout_fields', 10);

function rde_setup() {

    require 'inc/class-rde-cart.php';

//    load_theme_textdomain( 'rde' );

    load_theme_textdomain( 'rde', get_template_directory() . '/languages' );

    add_theme_support( 'woocommerce' );

    add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

    add_action( 'widgets_init', 'load_widgets' );

    // Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	add_image_size( 'logo-h90', null, 90, false  );

	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'rde' ),
        'mobile' => __( 'Mobile Menu', 'rde' ),
        'footer'    => __( 'Footer Menu', 'rde' ),
		'social' => __( 'Social Links Menu', 'rde' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
//	add_theme_support( 'customize-selective-refresh-widgets' );

}

add_action( 'after_setup_theme', 'rde_setup' );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'current ';
    }
    return $classes;
}

class Mobile_Menu_Walker extends Walker_Nav_Menu {
    function walk( $elements, $max_depth ) {
        $el_in_col = 6;
        $output = '';
        if( function_exists('wc_get_page_id') )
            $cart_id = wc_get_page_id( 'cart' );
//        echo print_r( $elements[6],1 );exit;
        if( $elements ) {
            foreach ($elements as $key=>$el) {
                $class = 'mainLink animated';
                if( $el->menu_item_parent )
                    $class = 'subLink animated';

                if( $key == 1 || ($key-1)%$el_in_col == 0 )
                    $output .= '<div class="col-md-6">';
                $output .= '<div class="'.$class.'"><a href="' . $el->url . '" class="' . join(' ', $el->classes) . '">'.($el->menu_item_parent?"<span>—</span>":"").$el->title.(($cart_id==$el->object_id)?"<span id='mobile_cart_items'>".WC()->cart->get_cart_contents_count()."</span>":"").'</a></div>';
                if( $key%$el_in_col == 0 || !isset($elements[$key+1]) )
                    $output .= '</div>';
            }
        }
        return $output;
    }
}

class Top_Menu_Walker extends Walker_Nav_Menu {

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );
        $wrap = '<div class="submenu"><div class="submenuContainer">';

        $classes = array( 'sub-menu' );

        $class_names = join( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $output .= "{$wrap}{$n}{$indent}<div class='triangle'></div><ul $class_names>{$n}";

    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );
        $wrap = '</div></div>';
        $output .= "$indent</ul>{$n}{$wrap}";
    }

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names .'>';

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $title = apply_filters( 'the_title', $item->title, $item->ID );

        $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . $title . $args->link_after;
        if( $this->has_children )
            $item_output .= '<span class="fa fa-sort-desc"></span>';
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

}

class Top_Menu_Social_Walker {
    function walk( $elements, $max_depth ) {

        $output = '';
        if( $elements ) {

            foreach ($elements as $el) {
                $output .= '<a target="_blank" href="' . $el->url . '" class="' . join(' ', $el->classes) . '"></a>';
            }
        }
        return $output;
    }
}

class Contact_Social_Walker {
    function walk( $elements, $max_depth ) {

        $output = '<ul>';
        if( $elements ) {

            foreach ($elements as $el) {
                $output .= '<li><a target="_blank" href="' . $el->url . '" class=""><span class="' . join(' ', $el->classes) . '"></span></a></li>';
            }
        }
        $output .= '</ul>';
        return $output;
    }
}

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if(!$numpages) {
            $numpages = 1;
        }
    }

    if ($numpages && $paged) {
        $pagenum_link = html_entity_decode( get_pagenum_link() );
        $url_parts    = explode( '?', $pagenum_link );

        if( ($paged-1)>=1 )
            $prev = $paged-1;
        if( ($paged+1)<=$numpages )
            $next = $paged+1;
        echo "<div class='pagination'><div class='paginationRow'>";
        echo "<div class='columnLeft'>";
        if( isset($prev) )
            echo "<a href='".add_query_arg('paged', $prev, $url_parts[0])."' class='prev'><span class='fa fa-chevron-left'></span>".__('Previous', 'rde')."</a>";
        echo "</div>";
        echo "<div class='columnCenter'>";
        if( $numpages && $numpages>1 ) {
            echo "<ul>";
            for ($i = 1; $i <= $numpages; $i++) {
                    echo "<li class='".(($i == $paged)?'current':'')."'><a href='" . add_query_arg('paged', $i, $url_parts[0]) . "'>" . $i . "</a></li>";
            }
            echo "</ul>";
        }
        echo "</div>";
        echo "<div class='columnRight'>";
        if( isset($next) )
            echo "<a href='".add_query_arg('paged',$next, $url_parts[0])."' class='next'>".__('Next', 'rde')."<span class='fa fa-chevron-right'></span></a>";
        echo "</div>";
        echo "</div></div>";
    }

}

if ( ! function_exists( 'woocommerce_form_field' ) ) {

    /**
     * Outputs a checkout/address form field.
     *
     * @subpackage    Forms
     *
     * @param string $key
     * @param mixed  $args
     * @param string $value (default: null)
     *
     * @return string
     */
    function rde_form_field( $key, $args, $value = null ) {
        $defaults = array(
            'type'              => 'text',
            'label'             => '',
            'description'       => '',
            'placeholder'       => '',
            'maxlength'         => false,
            'required'          => false,
            'autocomplete'      => false,
            'id'                => $key,
            'class'             => array(),
            'label_class'       => array(),
            'input_class'       => array(),
            'return'            => false,
            'options'           => array(),
            'custom_attributes' => array(),
            'validate'          => array(),
            'default'           => '',
            'autofocus'         => '',
            'priority'          => '',
        );

        $args = wp_parse_args( $args, $defaults );
        $args = apply_filters( 'woocommerce_form_field_args', $args, $key, $value );

        if ( $args['required'] ) {
            $args['class'][] = 'validate-required';
            $required = ' <abbr class="required" title="' . esc_attr__( 'required', 'woocommerce' ) . '">*</abbr>';
        } else {
            $required = '';
        }

        if ( is_string( $args['label_class'] ) ) {
            $args['label_class'] = array( $args['label_class'] );
        }

        if ( is_null( $value ) ) {
            $value = $args['default'];
        }

        // Custom attribute handling
        $custom_attributes         = array();
        $args['custom_attributes'] = array_filter( (array) $args['custom_attributes'] );

        if ( $args['maxlength'] ) {
            $args['custom_attributes']['maxlength'] = absint( $args['maxlength'] );
        }

        if ( ! empty( $args['autocomplete'] ) ) {
            $args['custom_attributes']['autocomplete'] = $args['autocomplete'];
        }

        if ( true === $args['autofocus'] ) {
            $args['custom_attributes']['autofocus'] = 'autofocus';
        }

        if ( ! empty( $args['custom_attributes'] ) && is_array( $args['custom_attributes'] ) ) {
            foreach ( $args['custom_attributes'] as $attribute => $attribute_value ) {
                $custom_attributes[] = esc_attr( $attribute ) . '="' . esc_attr( $attribute_value ) . '"';
            }
        }

        if ( ! empty( $args['validate'] ) ) {
            foreach ( $args['validate'] as $validate ) {
                $args['class'][] = 'validate-' . $validate;
            }
        }

        $field           = '';
        $label_id        = $args['id'];
        $sort            = $args['priority'] ? $args['priority'] : '';
        $field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr( $sort ) . '">%3$s</p>';

        switch ( $args['type'] ) {
            case 'country' :

                $countries = 'shipping_country' === $key ? WC()->countries->get_shipping_countries() : WC()->countries->get_allowed_countries();

                if ( 1 === sizeof( $countries ) ) {

                    $field .= '<strong>' . current( array_values( $countries ) ) . '</strong>';

                    $field .= '<input type="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="' . current( array_keys( $countries ) ) . '" ' . implode( ' ', $custom_attributes ) . ' class="country_to_state" readonly="readonly" />';

                } else {

                    $field = '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="country_to_state country_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . '>' . '<option value="">' . esc_html__( 'Select a country&hellip;', 'woocommerce' ) . '</option>';

                    foreach ( $countries as $ckey => $cvalue ) {
                        $field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
                    }

                    $field .= '</select>';

                    $field .= '<noscript><input type="submit" name="woocommerce_checkout_update_totals" value="' . esc_attr__( 'Update country', 'woocommerce' ) . '" /></noscript>';

                }

                break;
            case 'state' :
                /* Get country this state field is representing */
                $for_country = isset( $args['country'] ) ? $args['country'] : WC()->checkout->get_value( 'billing_state' === $key ? 'billing_country' : 'shipping_country' );
                $states      = WC()->countries->get_states( $for_country );

                if ( is_array( $states ) && empty( $states ) ) {

                    $field_container = '<p class="form-row %1$s" id="%2$s" style="display: none">%3$s</p>';

                    $field .= '<input type="hidden" class="hidden" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="" ' . implode( ' ', $custom_attributes ) . ' placeholder="' . esc_attr( $args['placeholder'] ) . '" readonly="readonly" />';

                } elseif ( ! is_null( $for_country ) && is_array( $states ) ) {

                    $field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="state_select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
						<option value="">' . esc_html__( 'Select a state&hellip;', 'woocommerce' ) . '</option>';

                    foreach ( $states as $ckey => $cvalue ) {
                        $field .= '<option value="' . esc_attr( $ckey ) . '" ' . selected( $value, $ckey, false ) . '>' . $cvalue . '</option>';
                    }

                    $field .= '</select>';

                } else {

                    $field .= '<input type="text" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $value ) . '"  placeholder="' . esc_attr( $args['placeholder'] ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" ' . implode( ' ', $custom_attributes ) . ' />';

                }

                break;
            case 'textarea' :

                $field .= '<textarea name="' . esc_attr( $key ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '" ' . ( empty( $args['custom_attributes']['rows'] ) ? ' rows="2"' : '' ) . ( empty( $args['custom_attributes']['cols'] ) ? ' cols="5"' : '' ) . implode( ' ', $custom_attributes ) . '>' . esc_textarea( $value ) . '</textarea>';

                break;
            case 'checkbox' :

                $field = '<input type="' . esc_attr( $args['type'] ) . '" class="input-checkbox ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" value="1" ' . checked( $value, 1, false ) . ' /> ';
                $field .= '<label for="'.$args['id'].'" class="checkbox ' . implode( ' ', $args['label_class'] ) . '" ' . implode( ' ', $custom_attributes ) . '><span></span>'.$args['label']. $required.'</label>';

                break;
            case 'password' :
            case 'text' :
            case 'email' :
            case 'tel' :
            case 'number' :

                $field .= '<input type="' . esc_attr( $args['type'] ) . '" class="input-text ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" placeholder="' . esc_attr( $args['placeholder'] ) . '"  value="' . esc_attr( $value ) . '" ' . implode( ' ', $custom_attributes ) . ' />';

                break;
            case 'select' :

                $options = $field = '';

                if ( ! empty( $args['options'] ) ) {
                    foreach ( $args['options'] as $option_key => $option_text ) {
                        if ( '' === $option_key ) {
                            // If we have a blank option, select2 needs a placeholder
                            if ( empty( $args['placeholder'] ) ) {
                                $args['placeholder'] = $option_text ? $option_text : __( 'Choose an option', 'woocommerce' );
                            }
                            $custom_attributes[] = 'data-allow_clear="true"';
                        }
                        $options .= '<option value="' . esc_attr( $option_key ) . '" ' . selected( $value, $option_key, false ) . '>' . esc_attr( $option_text ) . '</option>';
                    }

                    $field .= '<select name="' . esc_attr( $key ) . '" id="' . esc_attr( $args['id'] ) . '" class="select ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" ' . implode( ' ', $custom_attributes ) . ' data-placeholder="' . esc_attr( $args['placeholder'] ) . '">
							' . $options . '
						</select>';
                }

                break;
            case 'radio' :

                $label_id = current( array_keys( $args['options'] ) );

                if ( ! empty( $args['options'] ) ) {
                    foreach ( $args['options'] as $option_key => $option_text ) {
                        $field .= '<input type="radio" class="input-radio ' . esc_attr( implode( ' ', $args['input_class'] ) ) . '" value="' . esc_attr( $option_key ) . '" name="' . esc_attr( $key ) . '" ' . implode( ' ', $custom_attributes ) . ' id="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '"' . checked( $value, $option_key, false ) . ' />';
                        $field .= '<label for="' . esc_attr( $args['id'] ) . '_' . esc_attr( $option_key ) . '" class="radio ' . implode( ' ', $args['label_class'] ) . '">' . $option_text . '</label>';
                    }
                }

                break;
        }

        if ( ! empty( $field ) ) {
            $field_html = '';

            if ( $args['label'] && 'checkbox' != $args['type'] ) {
                $field_html .= '<label for="' . esc_attr( $label_id ) . '" class="' . esc_attr( implode( ' ', $args['label_class'] ) ) . '">' . $args['label'] . $required . '</label>';
            }

            $field_html .= $field;

            if ( $args['description'] ) {
                $field_html .= '<span class="description">' . esc_html( $args['description'] ) . '</span>';
            }

            $container_class = esc_attr( implode( ' ', $args['class'] ) );
            $container_id    = esc_attr( $args['id'] ) . '_field';
            $field           = sprintf( $field_container, $container_class, $container_id, $field_html );
        }

        $field = apply_filters( 'woocommerce_form_field_' . $args['type'], $field, $key, $args, $value );

        if ( $args['return'] ) {
            return $field;
        } else {
            echo $field;
        }
    }
}


/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Name', 'rde').':</strong> ' . $order->get_meta( '_billing_name' ) . '</p>';
    echo '<p><strong>'.__('Event venue', 'rde').':</strong> ' . $order->get_meta( '_billing_event_venue' ) . '</p>';
    echo '<p><strong>'.__('Event name', 'rde').':</strong> ' . $order->get_meta( '_billing_event_name' ) . '</p>';
    echo '<p><strong>'.__('Date of recepit', 'rde').':</strong> ' . $order->get_meta( '_billing_date_start' ) . '</p>';
    echo '<p><strong>'.__('Date of return', 'rde').':</strong> ' . $order->get_meta( '_billing_date_end' ) . '</p>';
    echo '<p><strong>'.__('Add transport service', 'rde').':</strong> ' . ($order->get_meta( '_billing_transport' )?__('Yes', 'rde'):__('No', 'rde')). '</p>';
}

function cart_needs_payment($this){
        return false;
}
add_filter('woocommerce_cart_needs_payment','cart_needs_payment', 10, 10);